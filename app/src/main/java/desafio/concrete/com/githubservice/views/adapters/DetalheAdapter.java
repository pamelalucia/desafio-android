package desafio.concrete.com.githubservice.views.adapters;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

import desafio.concrete.com.githubservice.R;
import desafio.concrete.com.githubservice.model.Pull;
import desafio.concrete.com.githubservice.util.Utils;

/**
 * Created by pamel on 17/12/2016.
 */

public class DetalheAdapter extends RecyclerView.Adapter<DetalheAdapter.ViewHolder> {
    public List<Pull> listPull = new ArrayList<Pull>();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView pullNome;
        public TextView username;
        public TextView body;
        public TextView data;

        public ImageView avatar;

        public ViewHolder(View v) {
            super(v);
            pullNome = (TextView) v.findViewById(R.id.pull_detalhe);
            body = (TextView) v.findViewById(R.id.body_detalhe);
            username = (TextView) v.findViewById(R.id.username_detalhe);
            avatar = (ImageView) v.findViewById(R.id.imagem_detalhe);
            data = (TextView) v.findViewById(R.id.data_detalhe);
        }
    }

    public DetalheAdapter(List<Pull> pulls) {
        listPull = pulls;
    }

    @Override
    public DetalheAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detalhe, parent, false);
        DetalheAdapter.ViewHolder vh = new DetalheAdapter.ViewHolder(mView);

        return vh;
    }

    @Override
    public void onBindViewHolder(DetalheAdapter.ViewHolder holder, int position) {
        final Pull pull = listPull.get(position);

        holder.pullNome.setText(pull.getTitle());
        holder.body.setText(pull.getBody());

        holder.username.setText(pull.getUser().getLogin());

        if(pull.getCriado() != null) {
            holder.data.setText(Utils.formatDate(pull.getCriado()));
        }
        Transformation transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(60)
                .oval(false)
                .build();

        if(pull.getUser().getAvatar() != null)
            Picasso.with(holder.avatar.getContext()).load(pull.getUser().getAvatar()).resize(100, 100).transform(transformation).into(holder.avatar);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = pull.getUrl();
                view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });

    }

    @Override
    public int getItemCount() {
        return listPull.size();
    }
}