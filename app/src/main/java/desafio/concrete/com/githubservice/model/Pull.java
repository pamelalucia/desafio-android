package desafio.concrete.com.githubservice.model;

import com.google.gson.annotations.SerializedName;


/**
 * Created by pamel on 17/12/2016.
 */

public class Pull {
    private String url;
    private String body;
    @SerializedName("created_at")
    private String criado;
    private User user;
    private String title;

    public Pull(String url, String body, String criado, User user, String title){
        this.url = url;
        this.body = body;
        this.criado = criado;
        this.user = user;
        this.title = title;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCriado() {
        return criado;
    }

    public void setCriado(String criado) {
        this.criado = criado;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}