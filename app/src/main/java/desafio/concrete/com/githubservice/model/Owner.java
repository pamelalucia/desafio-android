package desafio.concrete.com.githubservice.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pamel on 16/12/2016.
 */

public class Owner {

    @SerializedName("avatar_url")
    private String avatar;
    private String login;
    private String name;

    public Owner(){}
    public Owner(String avatar, String login, String name){
        this.avatar = avatar;
        this.login = login;
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
