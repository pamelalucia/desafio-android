package desafio.concrete.com.githubservice.views.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

import desafio.concrete.com.githubservice.DetalheActivity;
import desafio.concrete.com.githubservice.R;
import desafio.concrete.com.githubservice.model.Repository;

/**
 * Created by pamel on 16/12/2016.
 */

public class RepositorioAdapter extends RecyclerView.Adapter<RepositorioAdapter.ViewHolder> {
    public List<Repository> repositorios = new ArrayList<Repository>();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nomeRepositorio;
        public TextView descricao;
        public TextView forks;
        public TextView score;
        public TextView username;

        public ImageView avatar;


        public ViewHolder(View v) {
            super(v);
            nomeRepositorio = (TextView) v.findViewById(R.id.nome_repo_text);
            descricao = (TextView) v.findViewById(R.id.descricao_repo_text);
            forks = (TextView) v.findViewById(R.id.fork_text);
            score = (TextView) v.findViewById(R.id.star_text);
            username = (TextView) v.findViewById(R.id.username_text);

            avatar = (ImageView) v.findViewById(R.id.avatar_usuario_img);
        }
    }

    public RepositorioAdapter(List<Repository> repos) {
        repositorios = repos;
    }

    @Override
    public RepositorioAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,  int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repositorio, parent, false);
        RepositorioAdapter.ViewHolder vh = new RepositorioAdapter.ViewHolder(mView);

        return vh;
    }

    @Override
    public void onBindViewHolder(final RepositorioAdapter.ViewHolder holder, int position) {
        Repository repo = repositorios.get(position);

        holder.nomeRepositorio.setText(repo.getName());
        holder.descricao.setText(repo.getDescription()+"...");
        holder.forks.setText(repo.getForks());
        holder.score.setText(repo.getScore());
        holder.username.setText(repo.getOwner().getLogin());

        Transformation transformation = new RoundedTransformationBuilder()
                .cornerRadiusDp(60)
                .oval(false)
                .build();

        Picasso.with(holder.avatar.getContext()).load(repo.getOwner().getAvatar()).resize(100, 100).transform(transformation).into(holder.avatar);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CharSequence username = holder.username.getText();
                CharSequence nomeRepositorio = holder.nomeRepositorio.getText();

                view.getContext().startActivity(new Intent(view.getContext(), DetalheActivity.class)
                        .putExtra("username", username).putExtra("repositorio", nomeRepositorio));
            }
        });

    }

    @Override
    public int getItemCount() {
        return repositorios.size();
    }
}