package desafio.concrete.com.githubservice;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pamel on 17/12/2016.
 */

public class ErrorActivity extends AppCompatActivity {

    @BindView(R.id.error_text) TextView errorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        ButterKnife.bind(this);

        Bundle extra = getIntent().getExtras();
        if( extra != null ){
            String tipo = extra.getString("tipo");
            if( tipo.equals("1") ){
                errorText.setText(R.string.error_conexao);
            }else if(tipo.equals("2")){
                errorText.setText(R.string.error_generico);
            } else if(tipo.equals("3")){
                errorText.setText("Excedido limite github");
            }

        }
    }
}