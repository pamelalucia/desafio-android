package desafio.concrete.com.githubservice;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import desafio.concrete.com.githubservice.model.ItemsRepository;
import desafio.concrete.com.githubservice.model.Owner;
import desafio.concrete.com.githubservice.model.Repository;
import desafio.concrete.com.githubservice.util.ApiService;
import desafio.concrete.com.githubservice.util.EndlessRecyclerViewScrollListener;
import desafio.concrete.com.githubservice.util.Utils;
import desafio.concrete.com.githubservice.views.adapters.RepositorioAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler) RecyclerView recycler;
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private EndlessRecyclerViewScrollListener scrollListener;
    private ApiService apiService;
    private Retrofit retrofit;

    private List<Repository> repos = new ArrayList<Repository>();
    private List<Repository> newRepos = new ArrayList<Repository>();

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        recycler.setHasFixedSize(true);


        mLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(mLayoutManager);
        recycler.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(this)
                        .color(R.color.dark_gray).size(2).build());

        if(!Utils.isOnline()){
            startActivity(new Intent(getApplicationContext(), ErrorActivity.class).putExtra("tipo", "1"));
        } else{
            retrofit = new Retrofit.Builder()
                    .baseUrl(Utils.URL_SERVICE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            apiService = retrofit.create(ApiService.class);

            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.show();

            loadNextDataFromApi(1);

            scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    loadNextDataFromApi(page);
                    final int curSize = mAdapter.getItemCount();
                    repos.addAll(newRepos);

                    view.post(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.notifyItemRangeInserted(curSize, repos.size() - 1);
                        }
                    });
                }
            };
            recycler.addOnScrollListener(scrollListener);
        }
    }

    public void loadNextDataFromApi(int offset) {
        Call<ItemsRepository> call = apiService.getRepositorios(String.valueOf(offset));

        call.enqueue(new Callback<ItemsRepository>() {
            @Override
            public void onResponse(Call<ItemsRepository> call, Response<ItemsRepository> response) {
                if(response.isSuccessful()) {
                    newRepos = response.body().getItems();
                    if (repos.size() == 0) {
                        repos.addAll(newRepos);

                        if (mProgressDialog.isShowing())
                            mProgressDialog.dismiss();

                        mAdapter = new RepositorioAdapter(repos);
                        recycler.setAdapter(mAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ItemsRepository> call, Throwable t) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();

                startActivity(new Intent(getApplicationContext(), ErrorActivity.class).putExtra("tipo", "2"));
            }
        });
    }
}