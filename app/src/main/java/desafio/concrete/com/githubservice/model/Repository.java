package desafio.concrete.com.githubservice.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pamel on 16/12/2016.
 */

public class Repository {
    private String id;
    private String name;
    private String description;
    private String forks;
    @SerializedName("stargazers_count")
    private String score;
    private Owner owner;

    public Repository(){}
    public Repository(String id, String name, String description, String forks, String score, Owner owner){
        this.id = id;
        this.name = name;
        this.description = description;
        this.forks = forks;
        this.score = score;
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getForks() {
        return forks;
    }

    public void setForks(String forks) {
        this.forks = forks;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}