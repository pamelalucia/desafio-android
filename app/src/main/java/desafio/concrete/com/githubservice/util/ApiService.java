package desafio.concrete.com.githubservice.util;

import java.util.List;

import desafio.concrete.com.githubservice.model.ItemsRepository;
import desafio.concrete.com.githubservice.model.Owner;
import desafio.concrete.com.githubservice.model.Pull;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by pamel on 16/12/2016.
 */

public interface ApiService {
    @GET("search/repositories?q=language:Java&sort=stars&access_token=e0bd60ae56562003afa9d3e004d375c64b509069")
    Call<ItemsRepository> getRepositorios(@Query("page") String page);

    @GET("repos/{username}/{repositorio}/pulls?access_token=e0bd60ae56562003afa9d3e004d375c64b509069")
    Call<List<Pull>> getPull(@Path("username") String username, @Path("repositorio") String repositorio);
}