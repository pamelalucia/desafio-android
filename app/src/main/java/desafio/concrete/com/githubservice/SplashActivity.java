package desafio.concrete.com.githubservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import desafio.concrete.com.githubservice.util.Utils;

/**
 * Created by pamel on 17/12/2016.
 */

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!Utils.isOnline()) {
            startActivity(new Intent(getApplicationContext(), ErrorActivity.class).putExtra("tipo", "1"));
        } else {
            startActivity(new Intent(this, MainActivity.class));
        }
        finish();
    }
}