package desafio.concrete.com.githubservice.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pamel on 17/12/2016.
 */
public class User {
    private String login;
    @SerializedName("avatar_url")
    private String avatar;

    public User(String login, String avatar){
        this.login = login;
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}