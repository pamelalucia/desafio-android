package desafio.concrete.com.githubservice;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import desafio.concrete.com.githubservice.model.Pull;
import desafio.concrete.com.githubservice.util.ApiService;
import desafio.concrete.com.githubservice.util.Utils;
import desafio.concrete.com.githubservice.views.adapters.DetalheAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pamel on 17/12/2016.
 */

public class DetalheActivity extends AppCompatActivity {

    @BindView(R.id.recycler_detalhe)
    RecyclerView recycler;

    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ApiService apiService;

    private List<Pull> listPull = new ArrayList<Pull>();
    private List<Pull> newPull = new ArrayList<Pull>();

    private ProgressDialog mProgressDialog;

    private String username;
    private String repositorio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);
        ButterKnife.bind(this);

        Bundle extra = getIntent().getExtras();
        if( extra != null ){
            username = extra.getString("username");
            repositorio = extra.getString("repositorio");

        }

        recycler.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(mLayoutManager);
        recycler.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(this)
                        .color(R.color.dark_gray).size(2).build());

        if(!Utils.isOnline()){
            startActivity(new Intent(getApplicationContext(), ErrorActivity.class).putExtra("tipo", "1"));
        } else{
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Utils.URL_SERVICE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            apiService = retrofit.create(ApiService.class);

            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.show();

            loadNextDataFromApi();
        }
    }

    public void loadNextDataFromApi() {
        Call<List<Pull>> call = apiService.getPull(username, repositorio);

        call.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                if(response.isSuccessful()) {
                    newPull = response.body();
                    if (listPull.size() == 0) {
                        listPull.addAll(newPull);
                        mAdapter = new DetalheAdapter(listPull);
                        recycler.setAdapter(mAdapter);
                    }

                    if (mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {
                startActivity(new Intent(getApplicationContext(), ErrorActivity.class).putExtra("tipo", "2"));
            }
        });
    }
}