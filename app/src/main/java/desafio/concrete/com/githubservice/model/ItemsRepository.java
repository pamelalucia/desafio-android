package desafio.concrete.com.githubservice.model;

import java.util.List;

/**
 * Created by pamel on 16/12/2016.
 */

public class ItemsRepository {
    private List<Repository> items;

    public ItemsRepository(){}
    public ItemsRepository(List<Repository> items){
        this.items = items;
    }

    public List<Repository> getItems() {
        return items;
    }

    public void setItems(List<Repository> items) {
        this.items = items;
    }
}
